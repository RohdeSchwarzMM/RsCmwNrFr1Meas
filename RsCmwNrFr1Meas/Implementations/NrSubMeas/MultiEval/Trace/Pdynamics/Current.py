from typing import List

from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal.ArgSingleSuppressed import ArgSingleSuppressed
from ......Internal.Types import DataType


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CurrentCls:
	"""Current commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("current", core, parent)

	def read(self) -> List[float]:
		"""SCPI: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent \n
		Snippet: value: List[float] = driver.nrSubMeas.multiEval.trace.pdynamics.current.read() \n
		Return the values of the power dynamics traces. Each value is sampled with 48 Ts, corresponding to 1.5625 us. The results
		of the current, average and maximum traces can be retrieved. See also 'View Power Dynamics'. \n
		Use RsCmwNrFr1Meas.reliability.last_value to read the updated reliability indicator. \n
			:return: power: float 2048 power values, from -1100 us to +2098.4375 us relative to the start of the measured active slot. The values have a spacing of 1.5625 us. The 705th value is at the start of the slot (0 us) . Unit: dBm"""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_bin_or_ascii_float_list_suppressed(f'READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent?', suppressed)
		return response

	def fetch(self) -> List[float]:
		"""SCPI: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent \n
		Snippet: value: List[float] = driver.nrSubMeas.multiEval.trace.pdynamics.current.fetch() \n
		Return the values of the power dynamics traces. Each value is sampled with 48 Ts, corresponding to 1.5625 us. The results
		of the current, average and maximum traces can be retrieved. See also 'View Power Dynamics'. \n
		Use RsCmwNrFr1Meas.reliability.last_value to read the updated reliability indicator. \n
			:return: power: float 2048 power values, from -1100 us to +2098.4375 us relative to the start of the measured active slot. The values have a spacing of 1.5625 us. The 705th value is at the start of the slot (0 us) . Unit: dBm"""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_bin_or_ascii_float_list_suppressed(f'FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent?', suppressed)
		return response
