Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: