RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:AVERage:RBINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:AVERage:RBINdex



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Iemission.Margin.Average.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: