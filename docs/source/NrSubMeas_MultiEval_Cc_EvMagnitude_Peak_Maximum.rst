Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: