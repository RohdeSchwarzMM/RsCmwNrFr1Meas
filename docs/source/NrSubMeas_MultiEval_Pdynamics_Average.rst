Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: