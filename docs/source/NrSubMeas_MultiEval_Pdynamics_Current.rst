Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: