Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.trace.cc.repcap_carrierComponent_get()
	driver.nrSubMeas.multiEval.trace.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Cc_EsFlatness.rst
	NrSubMeas_MultiEval_Trace_Cc_Evmc.rst
	NrSubMeas_MultiEval_Trace_Cc_EvmSymbol.rst
	NrSubMeas_MultiEval_Trace_Cc_Iemissions.rst
	NrSubMeas_MultiEval_Trace_Cc_Iq.rst