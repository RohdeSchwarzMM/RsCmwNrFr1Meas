Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:LIMit
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:LIMit

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:LIMit
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:LIMit



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Iemissions.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: