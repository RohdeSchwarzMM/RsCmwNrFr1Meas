Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: