Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Margin.Minimum.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: