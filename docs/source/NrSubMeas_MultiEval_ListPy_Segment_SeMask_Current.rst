Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: