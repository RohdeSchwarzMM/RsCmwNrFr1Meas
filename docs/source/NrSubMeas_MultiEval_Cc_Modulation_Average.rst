Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: