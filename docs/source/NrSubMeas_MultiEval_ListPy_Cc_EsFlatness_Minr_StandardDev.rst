StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:SDEViation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:SDEViation



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.Minr.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: