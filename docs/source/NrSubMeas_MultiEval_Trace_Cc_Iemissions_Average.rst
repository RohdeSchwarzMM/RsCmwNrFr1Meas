Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Iemissions.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: