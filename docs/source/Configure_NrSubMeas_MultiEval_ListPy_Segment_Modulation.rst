Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:MODulation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:MODulation



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: