RfSettings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:RFSettings:CONNector

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:RFSettings:CONNector



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: