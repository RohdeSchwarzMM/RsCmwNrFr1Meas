Merror
----------------------------------------





.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Merror_Average.rst
	NrSubMeas_MultiEval_Cc_Merror_Current.rst
	NrSubMeas_MultiEval_Cc_Merror_Maximum.rst