Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrSubMeas.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrSubMeas_Scenario_Cspath.rst
	Route_NrSubMeas_Scenario_MaProtocol.rst
	Route_NrSubMeas_Scenario_Salone.rst