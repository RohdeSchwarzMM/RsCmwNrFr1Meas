Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: