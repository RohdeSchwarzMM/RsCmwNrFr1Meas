EsFlatness
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:ESFLatness
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:ESFLatness

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:ESFLatness
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:ESFLatness



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex: