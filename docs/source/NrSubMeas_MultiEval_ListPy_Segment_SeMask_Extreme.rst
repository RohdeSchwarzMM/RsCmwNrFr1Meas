Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: