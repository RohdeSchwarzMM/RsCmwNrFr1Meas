RsCmwNrFr1Meas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwNrFr1Meas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwNrFr1Meas.RsCmwNrFr1Meas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	NrSubMeas.rst
	Route.rst
	Sense.rst
	Trigger.rst