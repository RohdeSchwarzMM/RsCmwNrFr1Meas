Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Pmonitor.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: