Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr>:POSitiv:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr>:POSitiv:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr>:POSitiv:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr>:POSitiv:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Utra.Positiv.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: