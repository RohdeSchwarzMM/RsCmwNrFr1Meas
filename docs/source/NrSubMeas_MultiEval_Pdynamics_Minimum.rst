Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:MINimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: