Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: