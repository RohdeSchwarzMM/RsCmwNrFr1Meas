AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:ACSPacing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:ACSPacing



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Caggregation.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: