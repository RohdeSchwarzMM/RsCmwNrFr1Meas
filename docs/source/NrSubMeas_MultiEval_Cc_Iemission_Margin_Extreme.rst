Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:EXTReme

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:EXTReme



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Iemission.Margin.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.iemission.margin.extreme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Iemission_Margin_Extreme_RbIndex.rst