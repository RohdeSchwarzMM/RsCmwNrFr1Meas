DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: