Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Margin.Average.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: