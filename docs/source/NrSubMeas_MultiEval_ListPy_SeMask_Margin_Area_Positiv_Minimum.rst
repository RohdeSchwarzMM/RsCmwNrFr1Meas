Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:MINimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:MINimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Margin.Area.Positiv.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: