Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:ACLR:CAGGregation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:ACLR:CAGGregation



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Aclr.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: