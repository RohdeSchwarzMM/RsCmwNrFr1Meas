Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:CATalog:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:MEValuation:CATalog:SOURce



.. autoclass:: RsCmwNrFr1Meas.Implementations.Trigger.NrSubMeas.MultiEval.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: