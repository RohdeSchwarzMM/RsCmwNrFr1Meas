NrSubMeas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.NrSubMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.nrSubMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_NrSubMeas_RfSettings.rst
	Route_NrSubMeas_Scenario.rst