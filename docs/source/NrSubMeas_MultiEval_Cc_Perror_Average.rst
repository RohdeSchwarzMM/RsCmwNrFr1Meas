Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: