Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IEMissions:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Iemissions.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: