Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:CBANdwidth

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:CBANdwidth



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: