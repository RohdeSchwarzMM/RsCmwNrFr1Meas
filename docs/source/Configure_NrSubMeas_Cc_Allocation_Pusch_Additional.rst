Additional
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:ADDitional

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:ADDitional



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Allocation.Pusch.Additional.AdditionalCls
	:members:
	:undoc-members:
	:noindex: