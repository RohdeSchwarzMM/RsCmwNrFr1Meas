StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:SDEViation



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: