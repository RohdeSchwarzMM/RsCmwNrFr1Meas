Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:NEGativ

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:AVERage:NEGativ



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Margin.Average.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: