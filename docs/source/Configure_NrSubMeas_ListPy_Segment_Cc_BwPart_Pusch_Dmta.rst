Dmta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:BWPart:PUSCh:DMTA

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:BWPart:PUSCh:DMTA



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.BwPart.Pusch.Dmta.DmtaCls
	:members:
	:undoc-members:
	:noindex: