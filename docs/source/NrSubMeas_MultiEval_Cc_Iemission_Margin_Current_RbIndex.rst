RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:CURRent:RBINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:IEMission:MARGin:CURRent:RBINdex



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Iemission.Margin.Current.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: