RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:MLOFfset

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:MLOFfset



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex: