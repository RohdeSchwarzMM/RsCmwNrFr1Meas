Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EsFlatness.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: