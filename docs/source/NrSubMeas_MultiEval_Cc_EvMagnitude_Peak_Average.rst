Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:PEAK:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: