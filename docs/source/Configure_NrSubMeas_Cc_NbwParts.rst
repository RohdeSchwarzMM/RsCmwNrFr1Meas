NbwParts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:NBWParts

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:NBWParts



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Cc.NbwParts.NbwPartsCls
	:members:
	:undoc-members:
	:noindex: