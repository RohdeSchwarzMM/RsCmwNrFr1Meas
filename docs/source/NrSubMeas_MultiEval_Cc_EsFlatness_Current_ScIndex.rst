ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:CURRent:SCINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:CURRent:SCINdex



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EsFlatness.Current.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: