Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: