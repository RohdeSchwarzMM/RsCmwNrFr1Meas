Pcomp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PCOMp

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PCOMp



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Pcomp.PcompCls
	:members:
	:undoc-members:
	:noindex: