TxBwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:CCALl:TXBWidth:SCSPacing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:CCALl:TXBWidth:SCSPacing



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Ccall.TxBwidth.TxBwidthCls
	:members:
	:undoc-members:
	:noindex: