Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:SCENario:SALone



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: