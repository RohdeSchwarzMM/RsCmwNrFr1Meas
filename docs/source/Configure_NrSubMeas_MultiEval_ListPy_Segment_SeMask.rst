SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:SEMask

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:SEMask



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: