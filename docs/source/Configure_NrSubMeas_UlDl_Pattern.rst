Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:ULDL:PATTern

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:ULDL:PATTern



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.UlDl.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex: