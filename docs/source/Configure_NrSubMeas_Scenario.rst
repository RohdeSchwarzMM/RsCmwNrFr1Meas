Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SCENario:ACTivate

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SCENario:ACTivate



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex: