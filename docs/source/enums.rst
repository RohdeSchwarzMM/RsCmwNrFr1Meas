Enums
=========

AllocatedSlots
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AllocatedSlots.ALL
	# All values (1x):
	ALL

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.OB1
	# Last value:
	value = enums.Band.OB99
	# All values (57x):
	OB1 | OB105 | OB12 | OB13 | OB14 | OB18 | OB2 | OB20
	OB24 | OB25 | OB255 | OB256 | OB26 | OB28 | OB3 | OB30
	OB34 | OB38 | OB39 | OB40 | OB41 | OB46 | OB47 | OB48
	OB5 | OB50 | OB51 | OB53 | OB65 | OB66 | OB7 | OB70
	OB71 | OB74 | OB75 | OB76 | OB77 | OB78 | OB79 | OB8
	OB80 | OB81 | OB82 | OB83 | OB84 | OB85 | OB86 | OB89
	OB90 | OB91 | OB92 | OB93 | OB94 | OB95 | OB97 | OB98
	OB99

BandwidthPart
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthPart.BWP0
	# All values (1x):
	BWP0

CarrierComponent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrierComponent.CC1
	# All values (2x):
	CC1 | CC2

CarrierPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrierPosition.LONR
	# All values (2x):
	LONR | RONR

ChannelBwidth
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ChannelBwidth.B005
	# Last value:
	value = enums.ChannelBwidth.B100
	# All values (15x):
	B005 | B010 | B015 | B020 | B025 | B030 | B035 | B040
	B045 | B050 | B060 | B070 | B080 | B090 | B100

ChannelBwidthB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBwidthB.B005
	# All values (4x):
	B005 | B010 | B015 | B020

ChannelTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeA.PUCCh
	# All values (2x):
	PUCCh | PUSCh

ChannelTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeB.OFF
	# All values (4x):
	OFF | ON | PUCCh | PUSCh

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

ConfigType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigType.T1
	# All values (2x):
	T1 | T2

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

DuplexModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DuplexModeB.FDD
	# All values (2x):
	FDD | TDD

Generator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Generator.DID
	# All values (2x):
	DID | PHY

Lagging
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Lagging.MS05
	# All values (3x):
	MS05 | MS25 | OFF

Leading
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Leading.MS25
	# All values (2x):
	MS25 | OFF

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MappingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MappingType.A
	# All values (2x):
	A | B

MaxLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxLength.DOUBle
	# All values (2x):
	DOUBle | SINGle

MeasFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasFilter.BANDpass
	# All values (2x):
	BANDpass | GAUSs

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.MELMode
	# All values (2x):
	MELMode | NORMal

MeasureSlot
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasureSlot.ALL
	# All values (6x):
	ALL | MS0 | MS1 | MS2 | MS3 | UDEF

MevLimit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MevLimit.STD
	# All values (2x):
	STD | UDEF

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.BPSK
	# All values (6x):
	BPSK | BPWS | Q16 | Q256 | Q64 | QPSK

ModulationScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationScheme.AUTO
	# All values (7x):
	AUTO | BPSK | BPWS | Q16 | Q256 | Q64 | QPSK

NbTrigger
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NbTrigger.M010
	# All values (4x):
	M010 | M020 | M040 | M080

NetworkSigVal
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NetworkSigVal.NS01
	# Last value:
	value = enums.NetworkSigVal.NS35
	# All values (33x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS11 | NS12 | NS13 | NS14 | NS15 | NS16
	NS17 | NS18 | NS19 | NS20 | NS21 | NS22 | NS23 | NS24
	NS25 | NS26 | NS27 | NS28 | NS29 | NS30 | NS31 | NS32
	NS35

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Periodicity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Periodicity.MS05
	# Last value:
	value = enums.Periodicity.MS5
	# All values (9x):
	MS05 | MS1 | MS10 | MS125 | MS2 | MS25 | MS3 | MS4
	MS5

PhaseComp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhaseComp.CAF
	# All values (3x):
	CAF | OFF | UDEF

PucchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PucchFormat.F1
	# All values (7x):
	F1 | F1A | F1B | F2 | F2A | F2B | F3

RbwA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwA.K030
	# All values (3x):
	K030 | M1 | PC1

RbwB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwB.K030
	# All values (6x):
	K030 | K100 | K400 | M1 | PC1 | PC2

RbwC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwC.K030
	# All values (3x):
	K030 | K400 | M1

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPNarrowband
	# All values (4x):
	IFPNarrowband | IFPower | OFF | ON

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.CSPath
	# All values (4x):
	CSPath | MAPRotocol | NAV | SALone

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SubCarrSpacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubCarrSpacing.S15K
	# All values (3x):
	S15K | S30K | S60K

SyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncMode.ENHanced
	# All values (4x):
	ENHanced | ESSLot | NORMal | NSSLot

TimeMask
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMask.GOO
	# All values (3x):
	GOO | PPSRs | SBLanking

