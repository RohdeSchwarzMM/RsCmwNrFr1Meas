Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MERRor:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: