Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: