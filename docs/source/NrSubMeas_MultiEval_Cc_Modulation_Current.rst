Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: