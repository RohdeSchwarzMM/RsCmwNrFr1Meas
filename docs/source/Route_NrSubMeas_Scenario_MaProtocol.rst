MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: