StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: