Dmta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTA

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTA



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pusch.Dmta.DmtaCls
	:members:
	:undoc-members:
	:noindex: