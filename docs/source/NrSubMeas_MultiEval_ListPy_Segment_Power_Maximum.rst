Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: