EvMagnitude
----------------------------------------





.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_EvMagnitude_Average.rst
	NrSubMeas_MultiEval_Cc_EvMagnitude_Current.rst
	NrSubMeas_MultiEval_Cc_EvMagnitude_Maximum.rst
	NrSubMeas_MultiEval_Cc_EvMagnitude_Peak.rst