Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:EXTReme



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EsFlatness.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: