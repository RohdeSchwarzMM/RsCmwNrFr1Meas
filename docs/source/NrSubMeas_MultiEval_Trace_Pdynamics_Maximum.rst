Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: