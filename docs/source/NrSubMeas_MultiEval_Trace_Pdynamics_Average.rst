Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: