EsFlatness
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:ESFLatness

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:ESFLatness



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qam.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex: