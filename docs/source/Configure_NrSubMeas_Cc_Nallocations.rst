Nallocations
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:NALLocations

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:NALLocations



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Nallocations.NallocationsCls
	:members:
	:undoc-members:
	:noindex: