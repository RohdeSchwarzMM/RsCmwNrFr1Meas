RsCmwNrFr1Meas Events
==========================

.. _Events:

Check the usage in the Getting Started chapter :ref:`here <GetingStarted_Events>`.

.. autoclass:: RsCmwNrFr1Meas.CustomFiles.events.Events()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
