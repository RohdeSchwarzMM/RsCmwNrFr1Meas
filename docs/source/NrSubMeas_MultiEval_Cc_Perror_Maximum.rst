Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:PERRor:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: