Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:MAXimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: