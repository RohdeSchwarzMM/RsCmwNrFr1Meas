Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qpsk.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: