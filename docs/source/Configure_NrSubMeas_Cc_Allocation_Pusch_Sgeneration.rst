Sgeneration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:SGENeration

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh:SGENeration



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Allocation.Pusch.Sgeneration.SgenerationCls
	:members:
	:undoc-members:
	:noindex: