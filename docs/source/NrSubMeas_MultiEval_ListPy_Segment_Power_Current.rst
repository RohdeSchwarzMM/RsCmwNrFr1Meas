Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: