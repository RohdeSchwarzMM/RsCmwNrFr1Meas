Cspath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:NRSub:MEASurement<Instance>:SCENario:CSPath

.. code-block:: python

	ROUTe:NRSub:MEASurement<Instance>:SCENario:CSPath



.. autoclass:: RsCmwNrFr1Meas.Implementations.Route.NrSubMeas.Scenario.Cspath.CspathCls
	:members:
	:undoc-members:
	:noindex: