Evmc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMC
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMC

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMC
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMC



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Evmc.EvmcCls
	:members:
	:undoc-members:
	:noindex: