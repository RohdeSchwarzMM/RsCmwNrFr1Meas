Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer:MINimum



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: