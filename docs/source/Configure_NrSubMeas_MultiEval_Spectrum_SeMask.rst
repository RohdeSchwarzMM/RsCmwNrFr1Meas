SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: