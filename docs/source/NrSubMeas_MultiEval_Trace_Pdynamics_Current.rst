Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: