Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:ESFLatness:MINR<nr>:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.Minr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: