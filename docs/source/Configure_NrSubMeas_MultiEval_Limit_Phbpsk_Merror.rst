Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:MERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:MERRor



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Phbpsk.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: