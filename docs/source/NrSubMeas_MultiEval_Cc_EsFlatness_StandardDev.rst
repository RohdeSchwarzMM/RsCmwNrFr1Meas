StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:ESFLatness:SDEViation



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EsFlatness.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: