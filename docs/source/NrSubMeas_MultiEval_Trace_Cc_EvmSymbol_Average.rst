Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:EVMSymbol:AVERage



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: