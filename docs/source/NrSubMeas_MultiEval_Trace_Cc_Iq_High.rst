High
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IQ:HIGH

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>]:IQ:HIGH



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Iq.High.HighCls
	:members:
	:undoc-members:
	:noindex: