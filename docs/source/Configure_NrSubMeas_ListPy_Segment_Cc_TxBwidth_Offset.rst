Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:TXBWidth:OFFSet

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:TXBWidth:OFFSet



.. autoclass:: RsCmwNrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.TxBwidth.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: