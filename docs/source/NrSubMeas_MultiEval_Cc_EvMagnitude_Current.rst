Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:EVMagnitude:CURRent



.. autoclass:: RsCmwNrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.EvMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: